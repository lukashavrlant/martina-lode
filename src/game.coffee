startNewGame = () -> 
    images = ['podklad2.jpg', 'podklad1.jpg', 'africa_pruhled.png', 'europe.png', 'africa.png', 'house.png', 'cross.png', 'fire.png', 'asia_pruhled.png', 'asia.png', 'north_pruhled.png', 'australia_pruhled.png', 'south_pruhled.png', 'skvrny/10_c.png', 'skvrny/11_c.png', 'skvrny/12_c.png', 'skvrny/13_c.png', 'skvrny/14_c.png', 'skvrny/15_c.png', 'skvrny/16_c.png', 'skvrny/17_c.png', 'skvrny/1_c.png', 'skvrny/2_c.png', 'skvrny/3_c.png', 'skvrny/4_c.png', 'skvrny/5_c.png', 'skvrny/6_c.png', 'skvrny/7_c.png', 'skvrny/8_c.png', 'skvrny/9_c.png', 'skvrny/10_zc.png', 'skvrny/11_zc.png', 'skvrny/12_zc.png', 'skvrny/13_zc.png', 'skvrny/14_zc.png', 'skvrny/15_zc.png', 'skvrny/16_zc.png', 'skvrny/17_zc.png', 'skvrny/1_zc.png', 'skvrny/2_zc.png', 'skvrny/3_zc.png', 'skvrny/4_zc.png', 'skvrny/5_zc.png', 'skvrny/6_zc.png', 'skvrny/7_zc.png', 'skvrny/8_zc.png', 'skvrny/9_zc.png']
    stage = new createjs.Stage("game")
    preloadImages images, stage, () -> initializeGame stage, playerVsComputer

initializeGame = (stage, playerVsComputer) ->
    stage.enableMouseOver()
    
    settings = 
        player: {}
        computer: {}
        strokeSize: 3
        cellcount: 12
        cellsize: 40
        totalships: 4
        computersShips: []
        animations: []
        bloodIcons: 17
        playerVsComputer: playerVsComputer
        colors:
            empty: '#eeeeee'
            hit: 'red'
            player: 'blue'
        descriptions:
            putships: 'Place your targets. Targets left: '
            winner:
                computer: 'Enemy destroyed all your targets!'
                player: 'You destroyed all targest of enemy!'
        continents: [   
                        # {name: 'africa', width: 300, height: 335}, 
                        {name: 'europe', width: 397, height: 311}, 
                        # {name: 'asia', width: 420, height: 300}, 
                        {name: 'north', width: 400, height: 272}, 
                        # {name: 'australia', width: 397, height: 293},
                        {name: 'south', width: 211, height: 335}
                    ]

    initializeValues settings
    placeBackground stage, settings
    placeContinents stage, settings
    $('#backtotop').click () -> animateToTop stage, settings
    stage.update()

initializeValues = (sett) ->
    sett.shipsleft = sett.totalships
    sett.choosing = yes
    sett.player.movesCount = 0
    sett.computer.movesCount = 0
    sett.computer.health = sett.totalships
    sett.player.health = sett.totalships
    sett.ingame = no

alphaAnimate = (stage, sett, object, interval, from, to, onComplete = () -> ) -> 
    object.alpha = from
    animation = setInterval (() -> 
            if (from < to and object.alpha >= to) or (from > to and object.alpha <= to)
                clearInterval animation
                onComplete()
            else
                object.alpha += interval
            stage.update()), 50
    sett.animations.push animation

getImage = (name) ->
    new createjs.Bitmap 'img/' + name

placeBackground = (stage, sett) ->
    background = getImage 'podklad2.jpg'
    stage.addChild background
    alphaAnimate stage, sett, background, 0.05, 0, 1, () ->
        if !sett.playerVsComputer
            selectContinent stage, sett, sett.continents[randomInt 0, sett.continents.length - 1]
    sett.backgroundDark = background

    sett.backgroundLight = getImage 'podklad1.jpg'
    sett.backgroundLight.alpha = 0
    stage.addChild sett.backgroundLight

    if sett.playerVsComputer
        sett.backgroundLight.addEventListener 'click', (e) ->
            if sett.ingame
                if sett.choosing
                    coord = getCoord sett.player.continent.boardPoint, sett, e.stageX, e.stageY
                    handleRightClick stage, sett, coord
                else
                    coord = getCoord sett.computer.continent.boardPoint, sett, e.stageX, e.stageY
                    handleLeftClick stage, sett, coord

placeContinents = (stage, sett) ->
    for cont, i in sett.continents
        contImage = getImage cont.name + '_pruhled.png'
        contImage.y = 60
        cont.image = contImage
        contImage.alpha = 0.01
        if sett.playerVsComputer
            contImage.addEventListener 'mouseover', ((contImage) -> () -> switchAlpha stage, contImage)(contImage)
            contImage.addEventListener 'mouseout', ((contImage) -> () -> switchAlpha stage, contImage)(contImage)
            contImage.addEventListener 'click', ((x) -> () -> selectContinent stage, sett, x)(cont)
        stage.addChild contImage
    createStatusBar stage, sett
    setStatusText stage, sett, 'Choose your favorite continent!'

switchAlpha = (stage, object) ->
    object.alpha = 1 - object.alpha
    stage.update()

selectContinent = (stage, sett, continent) -> 
    for cont in sett.continents
        cont.image.alpha = 0
        cont.image.removeAllEventListeners()
    stage.update()
    sett.player.continent = continent
    sett.computer.continent = chooseContinent sett, continent
    showShipsLeft stage, sett
    placePlayersContinents stage, sett
    sett.ingame = yes

findXBorders = (object, y) -> 
    left = []
    right = []
    out = yes
    for x in [0..960]
        out = testBorders left, right, x, y, out, object
    {left:left, right:right}


findYBorders = (object, x) -> 
    left = []
    right = []
    out = yes
    for y in [0..660]
        out = testBorders left, right, x, y, out, object
    {left:left, right:right}

testBorders = (left, right, x, y, out, object) ->
    if object.hitTest x, y
        if out
            left.push new createjs.Point(x, y)
            return no
    else
        if !out
            right.push new createjs.Point(x, y)
            return yes
    return out

countContinentCoord = (continent, player) ->
    y = (660 - continent.height) / 2
    if player == 'player'
        x = (960 / 2)
    else
        x = 10
    return {x:x, y:y}

createContinent = (sett, player) -> 
    compCont = getImage sett[player].continent.name + '.png'
    sett[player].continent.bigImage = compCont

    compBoardPoint = countContinentCoord sett[player].continent, player

    compCont.set compBoardPoint   
    sett[player].continent.boardPoint = compBoardPoint
    compCont

existCell = (list, x, y) ->
    for cell in list when cell.row == x and cell.col == y
        return true
    return false

checkAndInitField = (sett, player, x, y) ->
    if existCell coordinates[sett[player].continent.name], x, y
        initField player, x, y
    else no

placePlayersContinents = (stage, sett) -> 
    alphaAnimate stage, sett, sett.backgroundDark, -0.05, 1, 0
    alphaAnimate stage, sett, sett.backgroundLight, 0.05, 0, 1

    playerCont = createContinent sett, 'player'
    compCont = createContinent sett, 'computer'

    container = new createjs.Container()
    container.addChild compCont
    container.addChild playerCont
    stage.addChild container

    sett.player.board = (checkAndInitField sett, 'player', x, y for x in [0..sett.cellcount] for y in [0..sett.cellcount])
    sett.computer.board = (checkAndInitField sett, 'computer', x, y for x in [0..sett.cellcount] for y in [0..sett.cellcount])

    if sett.playerVsComputer
        playerCont.addEventListener 'click', (e) ->
            c = getCoord sett.player.continent.boardPoint, sett, e.stageX, e.stageY
            handleRightClick stage, sett, c

        compCont.addEventListener 'click', (e) ->
            c = getCoord sett.computer.continent.boardPoint, sett, e.stageX, e.stageY
            handleLeftClick stage, sett, c

    animateBigContinents stage, sett, container
    stage.update()

doBoth = (fun) ->
    for pl in ['player', 'computer']
        fun pl

addStates = (sett, player) -> 
    if states[sett[player].continent.name]
        for info in states[sett[player].continent.name]
            field = getField sett, player, info.coord
            if field
                field.states = info.states
                shuffle field.states
                # field.house.alpha = 1
                # console.log field.states, info.states.length
        forEachBoard sett, player, (field, coord) ->
            if !field.states
                console.log 'chybi stat', coord
                # field.house.alpha = 1

animateBigContinents = (stage, sett, container) ->
    alphaAnimate stage, sett, container, 0.05, 0, 1, () ->
        addLines stage, sett, sett.computer.continent
        addLines stage, sett, sett.player.continent
        addLabels stage
        initIcons stage, sett
        doBoth (pl) -> addStates sett, pl
        if !sett.playerVsComputer
            initializeComputer stage, sett, 'computer'
            initializeComputer stage, sett, 'player'
            addTargetIcons stage, sett, 'player'
            addTargetIcons stage, sett, 'computer'
            startComputerGame stage, sett
        stage.update()

addLabels = (stage) ->
    enemy = new createjs.Text('ENEMY', "50px Helvetica", "#4b4a4a")
    enemy.x = 100
    enemy.y = 90

    you = new createjs.Text('YOU', "50px Helvetica", "#4b4a4a")
    you.x = 650
    you.y = 90

    stage.addChild you
    stage.addChild enemy


startComputerGame = (stage, sett) ->
    playComputer stage, sett, 'player'

addLines = (stage, sett, continent) -> 
    horizontal = {}
    vertical = {}
    objcoord = {x:continent.bigImage.x, y:continent.bigImage.y}

    for y in [0..660/sett.cellsize]
        borders = lineCoordinates[continent.name].horizontal[sett.cellsize * y]
        # borders = findXBorders continent.bigImage, sett.cellsize * y
        # horizontal[sett.cellsize * y] = borders
        stage.addChild makeLineThroughContinent borders, objcoord

    for x in [0..960/sett.cellsize]
        borders = lineCoordinates[continent.name].vertical[sett.cellsize * x]
        # borders = findYBorders continent.bigImage, sett.cellsize * x
        # vertical[sett.cellsize * x] = borders
        stage.addChild makeLineThroughContinent borders, objcoord

    # document.getElementById('output').value = JSON.stringify horizontal
    # document.getElementById('output2').value = JSON.stringify vertical

makeLineThroughContinent = (points, objcoord) ->
    container = new createjs.Container()
    if points.left.length > 0
        for i in [0..points.left.length-1]
            container.addChild makeLine moveCoord(points.left[i], objcoord), moveCoord(points.right[i], objcoord)
    container

makeLine = (from, to, stroke=1) ->
    g = new createjs.Graphics()
    g.setStrokeStyle 1
    g.beginStroke(createjs.Graphics.getRGB(0,0,100, 0.2))
    g.moveTo from.x, from.y
    g.lineTo to.x, to.y
    new createjs.Shape(g)

chooseContinent = (sett, except) ->
    rand = randomInt 0, sett.continents.length
    if sett.continents[rand].name != except.name
        sett.continents[rand]
    else
        chooseContinent sett, except

preloadImages = (images, stage, handler) ->
    progressItems = []
    progressItems.push createProgressBar stage
    preload = new createjs.LoadQueue(false, 'img/');
    preload.addEventListener("complete", () -> 
        for x in progressItems
            stage.removeChild x
        handler())
    for p in images
        preload.loadFile p, false
    preload.addEventListener 'progress', (e) -> 
        progressItems.push showProgress stage, e
    preload.load()

createProgressBar = (stage) ->
    rect = new createjs.Shape()
    rect.graphics.beginFill("black").drawRect(180, 300, 600, 30)
    rect.graphics.beginFill("white").drawRect(182, 302, 596, 26)
    stage.addChild rect
    stage.update()
    rect

showProgress = (stage, e) ->
    progressBar = new createjs.Shape()
    progressBar.graphics.beginFill("black").drawRect(182, 302, 596 * e.loaded, 26)
    stage.addChild progressBar
    stage.update()
    progressBar

initIcons = (stage, sett) ->
    sett.thinking = []
    container = new createjs.Container()
    for i in [1..8]
        thinking = getImage 'loader/frame-' + i + '.gif' 
        thinking.x = 300
        thinking.y = 110
        thinking.alpha = 0
        sett.thinking.push thinking
        container.addChild thinking
    stage.addChild container
    stage.update()

    for name in ['house']
        addIcons stage, sett, name

    addBloodIcon stage, sett, 'player'
    addBloodIcon stage, sett, 'computer'

getNumberIcons = (bloodIconNumbers, length) ->
    numbers = []
    for _ in [0..length / bloodIconNumbers]
        for x in [1..bloodIconNumbers]
            numbers.push x
    numbers

addBloodIcon = (stage, sett, player) ->
    length = coordinates[sett[player].continent.name].length
    numbers = getNumberIcons sett.bloodIcons, length
    shuffle numbers
    i = 0
    forEachBoard sett, player, (field, coord) ->
        point = getXY sett, coord, sett[player].continent.boardPoint
        number = numbers[i]
        i++
        grayIconName = number + "_zc.png"
        icon = createIcon grayIconName, point, sett, 'img/skvrny/'
        field['fire'] = icon
        stage.addChild icon
        redIconName = number + "_c.png"
        icon = createIcon redIconName, point, sett, 'img/skvrny/'
        field['cross'] = icon
        stage.addChild icon


addIcon = (stage, sett, name, player) ->
    forEachBoard sett, player, (field, coord) -> 
        point = getXY sett, coord, sett[player].continent.boardPoint
        icon = createIcon name + '.png', point, sett
        field[name] = icon
        stage.addChild icon

addIcons = (stage, sett, name) ->
    addIcon stage, sett, name, 'player'
    addIcon stage, sett, name, 'computer'

createIcon = (name, point, sett, directory = 'img/') ->
    icon = new createjs.Bitmap(directory + name)
    icon.scaleX = sett.cellsize / 60
    icon.scaleY = sett.cellsize / 60
    icon.x = point.x + 1
    icon.y = point.y + 1
    icon.alpha = 0
    icon

initField = (player, col, row) ->
    {
        chosen: no
        hit: no
        player: player
        col: col
        row: row
        house: no
    }

getXY = (sett, {col, row}, boardPoint) ->
    {
        x: boardPoint.x + col * sett.cellsize + 1
        y: boardPoint.y + row * sett.cellsize + 1
    }


getCoord = (boardPoint, sett, x, y) -> 
    x -= boardPoint.x
    y -= boardPoint.y
    x /= sett.cellsize
    y /= sett.cellsize
    x = Math.floor x
    y = Math.floor y
    {col:x, row:y}

getField = (sett, player, {col, row}) ->
    if col < 0 or row < 0 or col >= sett.cellsize or row >= sett.cellsize
        no
    else
        sett[player].board[col][row]

output = (text) ->
    out = document.getElementById('output')
    if out
        out.value += text

handleRightClick = (stage, sett, coord) ->
    # output JSON.stringify(coord) + ', '
    if sett.ingame
        if sett.choosing
            field = getField sett, 'player', coord
            if field
                if field.chosen
                    return
                    sett.shipsleft++
                else
                    sett.shipsleft--
                field.house.alpha = 1 - field.house.alpha
                field.chosen = !field.chosen
                showShipsLeft stage, sett

            if sett.shipsleft == 0
                sett.choosing = no
                setStatusText stage, sett, 'Attack the enemy!'
                initializeComputer stage, sett
                addTargetIcons stage, sett, 'player'
                addTargetIcons stage, sett, 'computer'
    stage.update()

forEachBoard = (sett, player, fun) ->
    for row, i in sett[player].board
        for x, j in row
            if x
                fun x, {col:i, row:j}

getOponent = (player) ->
    if player == 'computer' 
        'player' 
    else 
        'computer'

copyListOfCoord = (list) ->
    ({row: x.row, col:x.col} for x in list)

initializeComputer = (stage, sett, player = 'computer') ->
    oponent = getOponent player
    allcoord = getContinentCoordinates sett[player].continent
    shuffle allcoord
    sett[player].ships = allcoord[0..sett.totalships-1]
    for p in sett[player].ships
        console.log p.row, p.col
        [row, col] = [p.row+1, p.col+1]
        output "[" + col + ", " + row + "]\n"
        field = getField sett, player, p
        field.chosen = yes
        field.house.alpha = 0.5

    sett[player].moves = getContinentCoordinates sett[oponent].continent
    shuffle sett[player].moves

addTargetIcons = (stage, sett, player) ->
    numbers = [1..sett.totalships]
    shuffle numbers
    i = 0
    forEachBoard sett, player, (field, coord) ->
        if field.chosen
            point = getXY sett, coord, sett[player].continent.boardPoint
            name = 'target' + numbers[i] + '.png'
            i++
            icon = createIcon name, point, sett, 'img/'
            icon.alpha = 0
            field['fire'] = icon
            stage.addChild icon

getContinentCoordinates = (continent) ->
    copyListOfCoord coordinates[continent.name]

getAllCoordinates = (upto) ->
    allcoord = []
    for x in [0..upto]
        for y in [0..upto]
            allcoord.push {row:x, col:y}
    allcoord

randomInt = (from, to) ->
    Math.floor(Math.random() * to) + from

shuffle = (a) ->
    if a.length < 2
        return
    for i in [a.length-1..1]
        j = Math.floor Math.random() * (i + 1)
        [a[i], a[j]] = [a[j], a[i]]
    a

handleLeftClick = (stage, sett, coord) ->
    if sett.ingame
        if sett.choosing
            setStatusText stage, sett, 'You cannot fire :-(. Place your ships first.'
        else
            field = getField sett, 'computer', coord
            if field
                if field.hit
                    setStatusText stage, sett, "You have already fired there! Don't waste your ammo!"
                else
                    field.hit = yes
                    if field.chosen
                        field.house.alpha = 0
                        field.fire.alpha = 1
                        sett['computer'].health--
                        if sett['computer'].health
                            setStatusText stage, sett, 'Nice one!'
                        else
                            showWinner stage, sett, 'player'
                    else
                        field.fire.alpha = 1
                        setStatusText stage, sett, 'You missed!'
                    if sett.ingame
                        playComputer stage, sett
    stage.update()

showWinner = (stage, sett, player) ->
    setStatusText stage, sett, sett.descriptions.winner[player]
    sett.ingame = no
    numcomp = showDeadPeople sett, 'computer'
    numplayer = showDeadPeople sett, 'player'
    doBoth (player) ->
        sett[player].records = getCV sett, player
        saveRecords sett, player, sett[player].records
    animateToResults sett
    if !sett.playerVsComputer
        console.log sett.playerVsComputer
        setTimeout (() -> animateToTop stage, sett), 10000

countDeadChildren = (sett, player) ->
    counter = 0
    forEachBoard sett, player, (field) ->
        if field.hit and !field.chosen
            counter++
    counter

saveRecords = (sett, player, records) ->
    playerDeaths = wordNumbers[countDeadChildren sett, 'player']
    computerDeaths = wordNumbers[countDeadChildren sett, 'computer']
    
    id = "#cv" + player
    if player == "computer"
        title = "During the destruction of enemy targets you did not hit " + computerDeaths + " times! Those " + computerDeaths + " hits killed " + computerDeaths + " innocent children!"
    else
        title = "Those " + playerDeaths + " hits killed " + playerDeaths + " innocent children!"
    html = "<b>" + title + "</b><ul>"
    for rec in records
        html += "<li>" + rec + "</li>"
    html += "</ul>"
    $(id).html html

getCV = (sett, player) -> 
    states = {}
    forEachBoard sett, player, (field, coord) ->
        if field.hit and !field.chosen
            temp = field.states[0]
            if states[temp]
                states[temp]++
            else
                states[temp] = 1
    console.log states
    getCVRecords states

getCVRecords = (states) ->
    records = []
    for state, count of states
        temp = cv[state]
        if !temp
            temp = ["temporary child", "temporary child", "temporary child", "temporary child", "temporary child", "temporary child", "temporary child", "temporary child", "temporary child", "temporary child", "temporary child", "temporary child"]
        shuffle temp
        for x in temp[0..count-1]
            records.push x + ", " + state
    records

animateToResults = (sett) ->
    setTimeout (() ->
        if !sett.playerVsComputer
            $("#backtotop").hide()
        $("#cv").show()
        $('html, body').animate({
             scrollTop: $("#cv").offset().top
        }, 4000)), 2500

animateToTop = (stage, sett) ->
    $('html, body').animate({
         scrollTop: $("#game").offset().top
    }, 2000)
    setTimeout (() -> vanishGame stage, sett), 2250

vanishGame = (stage, sett) ->
    container = []
    for child in stage.children
        if child != sett.backgroundLight
            container.push child
    stage.update()

    count = 20
    anim = setInterval (() ->
            for c in container
                c.alpha -= 0.05
            count--
            stage.update()
            if count == 0
                clearInterval anim
                words = showFinalWords stage, sett
                alphaAnimate stage, sett, words, 0.05, 0, 1, () ->
                    for c in container
                        stage.removeChild c
                    setTimeout (() ->
                        alphaAnimate stage, sett, words, -0.05, 1, 0, (() ->
                                                stage.removeChild words
                                                pb = createReverseProgressBar stage
                                                showReverseProgress stage, sett, pb)), 5000
                    ), 50

showFinalWords = (stage, sett) ->
    text = new createjs.Text('This game was created on the occasion of the \nInternational Day of Innocent Children Victims of Aggression.\nThe day was recognized in 1983 by UN.', "25px Helvetica", "#4b4a4a")
    text.x = 500
    text.y = 200
    text.alpha = 0
    text.textAlign = 'center'
    stage.addChild text
    stage.update()
    text

max = (a, b) ->
    if a > b
        a
    else
        b

showReverseProgress = (stage, sett, progressBar) ->
    rect = new createjs.Shape()
    stage.addChild rect
    count = 60
    anim = setInterval (() ->
            rect.graphics.beginFill("white").drawRect(182, 302, 596, 26)
            rect.graphics.beginFill("black").drawRect(182, 302, max(10 * count - 4, 0), 26)
            stage.update()
            count--
            if count < 0
                stage.removeChild rect
                stage.removeChild progressBar
                clearInterval anim
                vanishStage stage, sett
                ), 100

vanishStage = (stage, sett) ->
    count = 10
    anim = setInterval (() ->
            if count < 0
                clearInterval anim
                stage.update()
                $('#cv').hide()
                if !sett.playerVsComputer
                    setTimeout (() ->
                        playsCounter++;
                        console.log playsCounter
                        if (playsCounter == 50) 
                            window.location.href = window.location.href
                        stage = new createjs.Stage("game")
                        initializeGame stage, no), 2000
            else
                count--
                for c in stage.children
                    c.alpha -= 0.1
                stage.update()), 50

createReverseProgressBar = (stage) ->
    rect = new createjs.Shape()
    rect.graphics.beginFill("black").drawRect(180, 300, 600, 30)
    stage.addChild rect
    stage.update()
    rect

showDeadPeople = (sett, player) ->
    forEachBoard sett, player, (field, coord) ->
        if field.hit and !field.chosen
            field.fire.alpha = 0
            field.cross.alpha = 1

moveCoord = (origin, shift) ->
    new createjs.Point(origin.x + shift.x, origin.y + shift.y)


playComputer = (stage, sett, player = 'computer') ->
    sett.ingame = no
    placeField stage, sett, sett[player].moves[sett[player].movesCount], player
    sett[player].movesCount++

placeField = (stage, sett, coord, player = 'computer') ->
    oponent = getOponent player
    field = getField sett, oponent, coord
    field.hit = yes
    if field.chosen
        sett[oponent].health--
    animation = animateThinking stage, sett

    fun = () ->
        stopAnimateThinking sett, animation
        field.house.alpha = 0
        field.fire.alpha = 1
        if sett[oponent].health == 0
            sett.ingame = no
            showWinner stage, sett, player
        
        if field.chosen
            setStatusText stage, sett, "Enemy destroyed one of your target!"
            
        stage.update()
        if sett.ingame and !sett.playerVsComputer
            playComputer stage, sett, oponent

    sett.animations.push setTimeout fun, randomInt 100, 200

animateThinking = (stage, sett) ->
    index = 0
    length = sett.thinking.length
    animation = setInterval (() ->
            for x in sett.thinking
                x.alpha = 0
            sett.thinking[index].alpha = 1
            stage.update()
            index = (index + 1) % length), 100
    sett.animations.push animation
    animation

stopAnimateThinking = (sett, animation) ->
    for x in sett.thinking
        x.alpha = 0
    clearInterval animation
    sett.ingame = yes

createStatusBar = (stage, sett) -> 
    text = new createjs.Text('Vitejte ve hre', "20px Helvetica", "#4b4a4a")
    text.x = 600
    text.y = 600
    stage.addChild text
    sett.statusbar = text

setStatusText = (stage, sett, text) ->
    sett.statusbar.text = text
    stage.update()

showShipsLeft = (stage, sett) ->
    setStatusText stage, sett, sett.descriptions.putships + sett.shipsleft

window.onload = () -> 
    startNewGame()