// Generated by CoffeeScript 1.6.1
(function() {
  var addBloodIcon, addIcon, addIcons, addLabels, addLines, addStates, addTargetIcons, alphaAnimate, animateBigContinents, animateThinking, animateToResults, animateToTop, checkAndInitField, chooseContinent, copyListOfCoord, countContinentCoord, countDeadChildren, createContinent, createIcon, createProgressBar, createReverseProgressBar, createStatusBar, doBoth, existCell, findXBorders, findYBorders, forEachBoard, getAllCoordinates, getCV, getCVRecords, getContinentCoordinates, getCoord, getField, getImage, getNumberIcons, getOponent, getXY, handleLeftClick, handleRightClick, initField, initIcons, initializeComputer, initializeGame, initializeValues, makeLine, makeLineThroughContinent, max, moveCoord, output, placeBackground, placeContinents, placeField, placePlayersContinents, playComputer, preloadImages, randomInt, saveRecords, selectContinent, setStatusText, showDeadPeople, showFinalWords, showProgress, showReverseProgress, showShipsLeft, showWinner, shuffle, startComputerGame, startNewGame, stopAnimateThinking, switchAlpha, testBorders, vanishGame, vanishStage;

  startNewGame = function() {
    var images, stage;
    images = ['podklad2.jpg', 'podklad1.jpg', 'africa_pruhled.png', 'europe.png', 'africa.png', 'house.png', 'cross.png', 'fire.png', 'asia_pruhled.png', 'asia.png', 'north_pruhled.png', 'australia_pruhled.png', 'south_pruhled.png', 'skvrny/10_c.png', 'skvrny/11_c.png', 'skvrny/12_c.png', 'skvrny/13_c.png', 'skvrny/14_c.png', 'skvrny/15_c.png', 'skvrny/16_c.png', 'skvrny/17_c.png', 'skvrny/1_c.png', 'skvrny/2_c.png', 'skvrny/3_c.png', 'skvrny/4_c.png', 'skvrny/5_c.png', 'skvrny/6_c.png', 'skvrny/7_c.png', 'skvrny/8_c.png', 'skvrny/9_c.png', 'skvrny/10_zc.png', 'skvrny/11_zc.png', 'skvrny/12_zc.png', 'skvrny/13_zc.png', 'skvrny/14_zc.png', 'skvrny/15_zc.png', 'skvrny/16_zc.png', 'skvrny/17_zc.png', 'skvrny/1_zc.png', 'skvrny/2_zc.png', 'skvrny/3_zc.png', 'skvrny/4_zc.png', 'skvrny/5_zc.png', 'skvrny/6_zc.png', 'skvrny/7_zc.png', 'skvrny/8_zc.png', 'skvrny/9_zc.png'];
    stage = new createjs.Stage("game");
    return preloadImages(images, stage, function() {
      return initializeGame(stage, playerVsComputer);
    });
  };

  initializeGame = function(stage, playerVsComputer) {
    var settings;
    stage.enableMouseOver();
    settings = {
      player: {},
      computer: {},
      strokeSize: 3,
      cellcount: 12,
      cellsize: 40,
      totalships: 4,
      computersShips: [],
      animations: [],
      bloodIcons: 17,
      playerVsComputer: playerVsComputer,
      colors: {
        empty: '#eeeeee',
        hit: 'red',
        player: 'blue'
      },
      descriptions: {
        putships: 'Place your targets. Targets left: ',
        winner: {
          computer: 'Enemy destroyed all your targets!',
          player: 'You destroyed all targest of enemy!'
        }
      },
      continents: [
        {
          name: 'europe',
          width: 397,
          height: 311
        }, {
          name: 'north',
          width: 400,
          height: 272
        }, {
          name: 'south',
          width: 211,
          height: 335
        }
      ]
    };
    initializeValues(settings);
    placeBackground(stage, settings);
    placeContinents(stage, settings);
    $('#backtotop').click(function() {
      return animateToTop(stage, settings);
    });
    return stage.update();
  };

  initializeValues = function(sett) {
    sett.shipsleft = sett.totalships;
    sett.choosing = true;
    sett.player.movesCount = 0;
    sett.computer.movesCount = 0;
    sett.computer.health = sett.totalships;
    sett.player.health = sett.totalships;
    return sett.ingame = false;
  };

  alphaAnimate = function(stage, sett, object, interval, from, to, onComplete) {
    var animation;
    if (onComplete == null) {
      onComplete = function() {};
    }
    object.alpha = from;
    animation = setInterval((function() {
      if ((from < to && object.alpha >= to) || (from > to && object.alpha <= to)) {
        clearInterval(animation);
        onComplete();
      } else {
        object.alpha += interval;
      }
      return stage.update();
    }), 50);
    return sett.animations.push(animation);
  };

  getImage = function(name) {
    return new createjs.Bitmap('img/' + name);
  };

  placeBackground = function(stage, sett) {
    var background;
    background = getImage('podklad2.jpg');
    stage.addChild(background);
    alphaAnimate(stage, sett, background, 0.05, 0, 1, function() {
      if (!sett.playerVsComputer) {
        return selectContinent(stage, sett, sett.continents[randomInt(0, sett.continents.length - 1)]);
      }
    });
    sett.backgroundDark = background;
    sett.backgroundLight = getImage('podklad1.jpg');
    sett.backgroundLight.alpha = 0;
    stage.addChild(sett.backgroundLight);
    if (sett.playerVsComputer) {
      return sett.backgroundLight.addEventListener('click', function(e) {
        var coord;
        if (sett.ingame) {
          if (sett.choosing) {
            coord = getCoord(sett.player.continent.boardPoint, sett, e.stageX, e.stageY);
            return handleRightClick(stage, sett, coord);
          } else {
            coord = getCoord(sett.computer.continent.boardPoint, sett, e.stageX, e.stageY);
            return handleLeftClick(stage, sett, coord);
          }
        }
      });
    }
  };

  placeContinents = function(stage, sett) {
    var cont, contImage, i, _i, _len, _ref;
    _ref = sett.continents;
    for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
      cont = _ref[i];
      contImage = getImage(cont.name + '_pruhled.png');
      contImage.y = 60;
      cont.image = contImage;
      contImage.alpha = 0.01;
      if (sett.playerVsComputer) {
        contImage.addEventListener('mouseover', (function(contImage) {
          return function() {
            return switchAlpha(stage, contImage);
          };
        })(contImage));
        contImage.addEventListener('mouseout', (function(contImage) {
          return function() {
            return switchAlpha(stage, contImage);
          };
        })(contImage));
        contImage.addEventListener('click', (function(x) {
          return function() {
            return selectContinent(stage, sett, x);
          };
        })(cont));
      }
      stage.addChild(contImage);
    }
    createStatusBar(stage, sett);
    return setStatusText(stage, sett, 'Choose your favorite continent!');
  };

  switchAlpha = function(stage, object) {
    object.alpha = 1 - object.alpha;
    return stage.update();
  };

  selectContinent = function(stage, sett, continent) {
    var cont, _i, _len, _ref;
    _ref = sett.continents;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      cont = _ref[_i];
      cont.image.alpha = 0;
      cont.image.removeAllEventListeners();
    }
    stage.update();
    sett.player.continent = continent;
    sett.computer.continent = chooseContinent(sett, continent);
    showShipsLeft(stage, sett);
    placePlayersContinents(stage, sett);
    return sett.ingame = true;
  };

  findXBorders = function(object, y) {
    var left, out, right, x, _i;
    left = [];
    right = [];
    out = true;
    for (x = _i = 0; _i <= 960; x = ++_i) {
      out = testBorders(left, right, x, y, out, object);
    }
    return {
      left: left,
      right: right
    };
  };

  findYBorders = function(object, x) {
    var left, out, right, y, _i;
    left = [];
    right = [];
    out = true;
    for (y = _i = 0; _i <= 660; y = ++_i) {
      out = testBorders(left, right, x, y, out, object);
    }
    return {
      left: left,
      right: right
    };
  };

  testBorders = function(left, right, x, y, out, object) {
    if (object.hitTest(x, y)) {
      if (out) {
        left.push(new createjs.Point(x, y));
        return false;
      }
    } else {
      if (!out) {
        right.push(new createjs.Point(x, y));
        return true;
      }
    }
    return out;
  };

  countContinentCoord = function(continent, player) {
    var x, y;
    y = (660 - continent.height) / 2;
    if (player === 'player') {
      x = 960 / 2;
    } else {
      x = 10;
    }
    return {
      x: x,
      y: y
    };
  };

  createContinent = function(sett, player) {
    var compBoardPoint, compCont;
    compCont = getImage(sett[player].continent.name + '.png');
    sett[player].continent.bigImage = compCont;
    compBoardPoint = countContinentCoord(sett[player].continent, player);
    compCont.set(compBoardPoint);
    sett[player].continent.boardPoint = compBoardPoint;
    return compCont;
  };

  existCell = function(list, x, y) {
    var cell, _i, _len;
    for (_i = 0, _len = list.length; _i < _len; _i++) {
      cell = list[_i];
      if (cell.row === x && cell.col === y) {
        return true;
      }
    }
    return false;
  };

  checkAndInitField = function(sett, player, x, y) {
    if (existCell(coordinates[sett[player].continent.name], x, y)) {
      return initField(player, x, y);
    } else {
      return false;
    }
  };

  placePlayersContinents = function(stage, sett) {
    var compCont, container, playerCont, x, y;
    alphaAnimate(stage, sett, sett.backgroundDark, -0.05, 1, 0);
    alphaAnimate(stage, sett, sett.backgroundLight, 0.05, 0, 1);
    playerCont = createContinent(sett, 'player');
    compCont = createContinent(sett, 'computer');
    container = new createjs.Container();
    container.addChild(compCont);
    container.addChild(playerCont);
    stage.addChild(container);
    sett.player.board = (function() {
      var _i, _ref, _results;
      _results = [];
      for (y = _i = 0, _ref = sett.cellcount; 0 <= _ref ? _i <= _ref : _i >= _ref; y = 0 <= _ref ? ++_i : --_i) {
        _results.push((function() {
          var _j, _ref1, _results1;
          _results1 = [];
          for (x = _j = 0, _ref1 = sett.cellcount; 0 <= _ref1 ? _j <= _ref1 : _j >= _ref1; x = 0 <= _ref1 ? ++_j : --_j) {
            _results1.push(checkAndInitField(sett, 'player', x, y));
          }
          return _results1;
        })());
      }
      return _results;
    })();
    sett.computer.board = (function() {
      var _i, _ref, _results;
      _results = [];
      for (y = _i = 0, _ref = sett.cellcount; 0 <= _ref ? _i <= _ref : _i >= _ref; y = 0 <= _ref ? ++_i : --_i) {
        _results.push((function() {
          var _j, _ref1, _results1;
          _results1 = [];
          for (x = _j = 0, _ref1 = sett.cellcount; 0 <= _ref1 ? _j <= _ref1 : _j >= _ref1; x = 0 <= _ref1 ? ++_j : --_j) {
            _results1.push(checkAndInitField(sett, 'computer', x, y));
          }
          return _results1;
        })());
      }
      return _results;
    })();
    if (sett.playerVsComputer) {
      playerCont.addEventListener('click', function(e) {
        var c;
        c = getCoord(sett.player.continent.boardPoint, sett, e.stageX, e.stageY);
        return handleRightClick(stage, sett, c);
      });
      compCont.addEventListener('click', function(e) {
        var c;
        c = getCoord(sett.computer.continent.boardPoint, sett, e.stageX, e.stageY);
        return handleLeftClick(stage, sett, c);
      });
    }
    animateBigContinents(stage, sett, container);
    return stage.update();
  };

  doBoth = function(fun) {
    var pl, _i, _len, _ref, _results;
    _ref = ['player', 'computer'];
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      pl = _ref[_i];
      _results.push(fun(pl));
    }
    return _results;
  };

  addStates = function(sett, player) {
    var field, info, _i, _len, _ref;
    if (states[sett[player].continent.name]) {
      _ref = states[sett[player].continent.name];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        info = _ref[_i];
        field = getField(sett, player, info.coord);
        if (field) {
          field.states = info.states;
          shuffle(field.states);
        }
      }
      return forEachBoard(sett, player, function(field, coord) {
        if (!field.states) {
          return console.log('chybi stat', coord);
        }
      });
    }
  };

  animateBigContinents = function(stage, sett, container) {
    return alphaAnimate(stage, sett, container, 0.05, 0, 1, function() {
      addLines(stage, sett, sett.computer.continent);
      addLines(stage, sett, sett.player.continent);
      addLabels(stage);
      initIcons(stage, sett);
      doBoth(function(pl) {
        return addStates(sett, pl);
      });
      if (!sett.playerVsComputer) {
        initializeComputer(stage, sett, 'computer');
        initializeComputer(stage, sett, 'player');
        addTargetIcons(stage, sett, 'player');
        addTargetIcons(stage, sett, 'computer');
        startComputerGame(stage, sett);
      }
      return stage.update();
    });
  };

  addLabels = function(stage) {
    var enemy, you;
    enemy = new createjs.Text('ENEMY', "50px Helvetica", "#4b4a4a");
    enemy.x = 100;
    enemy.y = 90;
    you = new createjs.Text('YOU', "50px Helvetica", "#4b4a4a");
    you.x = 650;
    you.y = 90;
    stage.addChild(you);
    return stage.addChild(enemy);
  };

  startComputerGame = function(stage, sett) {
    return playComputer(stage, sett, 'player');
  };

  addLines = function(stage, sett, continent) {
    var borders, horizontal, objcoord, vertical, x, y, _i, _j, _ref, _ref1, _results;
    horizontal = {};
    vertical = {};
    objcoord = {
      x: continent.bigImage.x,
      y: continent.bigImage.y
    };
    for (y = _i = 0, _ref = 660 / sett.cellsize; 0 <= _ref ? _i <= _ref : _i >= _ref; y = 0 <= _ref ? ++_i : --_i) {
      borders = lineCoordinates[continent.name].horizontal[sett.cellsize * y];
      stage.addChild(makeLineThroughContinent(borders, objcoord));
    }
    _results = [];
    for (x = _j = 0, _ref1 = 960 / sett.cellsize; 0 <= _ref1 ? _j <= _ref1 : _j >= _ref1; x = 0 <= _ref1 ? ++_j : --_j) {
      borders = lineCoordinates[continent.name].vertical[sett.cellsize * x];
      _results.push(stage.addChild(makeLineThroughContinent(borders, objcoord)));
    }
    return _results;
  };

  makeLineThroughContinent = function(points, objcoord) {
    var container, i, _i, _ref;
    container = new createjs.Container();
    if (points.left.length > 0) {
      for (i = _i = 0, _ref = points.left.length - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        container.addChild(makeLine(moveCoord(points.left[i], objcoord), moveCoord(points.right[i], objcoord)));
      }
    }
    return container;
  };

  makeLine = function(from, to, stroke) {
    var g;
    if (stroke == null) {
      stroke = 1;
    }
    g = new createjs.Graphics();
    g.setStrokeStyle(1);
    g.beginStroke(createjs.Graphics.getRGB(0, 0, 100, 0.2));
    g.moveTo(from.x, from.y);
    g.lineTo(to.x, to.y);
    return new createjs.Shape(g);
  };

  chooseContinent = function(sett, except) {
    var rand;
    rand = randomInt(0, sett.continents.length);
    if (sett.continents[rand].name !== except.name) {
      return sett.continents[rand];
    } else {
      return chooseContinent(sett, except);
    }
  };

  preloadImages = function(images, stage, handler) {
    var p, preload, progressItems, _i, _len;
    progressItems = [];
    progressItems.push(createProgressBar(stage));
    preload = new createjs.LoadQueue(false, 'img/');
    preload.addEventListener("complete", function() {
      var x, _i, _len;
      for (_i = 0, _len = progressItems.length; _i < _len; _i++) {
        x = progressItems[_i];
        stage.removeChild(x);
      }
      return handler();
    });
    for (_i = 0, _len = images.length; _i < _len; _i++) {
      p = images[_i];
      preload.loadFile(p, false);
    }
    preload.addEventListener('progress', function(e) {
      return progressItems.push(showProgress(stage, e));
    });
    return preload.load();
  };

  createProgressBar = function(stage) {
    var rect;
    rect = new createjs.Shape();
    rect.graphics.beginFill("black").drawRect(180, 300, 600, 30);
    rect.graphics.beginFill("white").drawRect(182, 302, 596, 26);
    stage.addChild(rect);
    stage.update();
    return rect;
  };

  showProgress = function(stage, e) {
    var progressBar;
    progressBar = new createjs.Shape();
    progressBar.graphics.beginFill("black").drawRect(182, 302, 596 * e.loaded, 26);
    stage.addChild(progressBar);
    stage.update();
    return progressBar;
  };

  initIcons = function(stage, sett) {
    var container, i, name, thinking, _i, _j, _len, _ref;
    sett.thinking = [];
    container = new createjs.Container();
    for (i = _i = 1; _i <= 8; i = ++_i) {
      thinking = getImage('loader/frame-' + i + '.gif');
      thinking.x = 300;
      thinking.y = 110;
      thinking.alpha = 0;
      sett.thinking.push(thinking);
      container.addChild(thinking);
    }
    stage.addChild(container);
    stage.update();
    _ref = ['house'];
    for (_j = 0, _len = _ref.length; _j < _len; _j++) {
      name = _ref[_j];
      addIcons(stage, sett, name);
    }
    addBloodIcon(stage, sett, 'player');
    return addBloodIcon(stage, sett, 'computer');
  };

  getNumberIcons = function(bloodIconNumbers, length) {
    var numbers, x, _, _i, _j, _ref;
    numbers = [];
    for (_ = _i = 0, _ref = length / bloodIconNumbers; 0 <= _ref ? _i <= _ref : _i >= _ref; _ = 0 <= _ref ? ++_i : --_i) {
      for (x = _j = 1; 1 <= bloodIconNumbers ? _j <= bloodIconNumbers : _j >= bloodIconNumbers; x = 1 <= bloodIconNumbers ? ++_j : --_j) {
        numbers.push(x);
      }
    }
    return numbers;
  };

  addBloodIcon = function(stage, sett, player) {
    var i, length, numbers;
    length = coordinates[sett[player].continent.name].length;
    numbers = getNumberIcons(sett.bloodIcons, length);
    shuffle(numbers);
    i = 0;
    return forEachBoard(sett, player, function(field, coord) {
      var grayIconName, icon, number, point, redIconName;
      point = getXY(sett, coord, sett[player].continent.boardPoint);
      number = numbers[i];
      i++;
      grayIconName = number + "_zc.png";
      icon = createIcon(grayIconName, point, sett, 'img/skvrny/');
      field['fire'] = icon;
      stage.addChild(icon);
      redIconName = number + "_c.png";
      icon = createIcon(redIconName, point, sett, 'img/skvrny/');
      field['cross'] = icon;
      return stage.addChild(icon);
    });
  };

  addIcon = function(stage, sett, name, player) {
    return forEachBoard(sett, player, function(field, coord) {
      var icon, point;
      point = getXY(sett, coord, sett[player].continent.boardPoint);
      icon = createIcon(name + '.png', point, sett);
      field[name] = icon;
      return stage.addChild(icon);
    });
  };

  addIcons = function(stage, sett, name) {
    addIcon(stage, sett, name, 'player');
    return addIcon(stage, sett, name, 'computer');
  };

  createIcon = function(name, point, sett, directory) {
    var icon;
    if (directory == null) {
      directory = 'img/';
    }
    icon = new createjs.Bitmap(directory + name);
    icon.scaleX = sett.cellsize / 60;
    icon.scaleY = sett.cellsize / 60;
    icon.x = point.x + 1;
    icon.y = point.y + 1;
    icon.alpha = 0;
    return icon;
  };

  initField = function(player, col, row) {
    return {
      chosen: false,
      hit: false,
      player: player,
      col: col,
      row: row,
      house: false
    };
  };

  getXY = function(sett, _arg, boardPoint) {
    var col, row;
    col = _arg.col, row = _arg.row;
    return {
      x: boardPoint.x + col * sett.cellsize + 1,
      y: boardPoint.y + row * sett.cellsize + 1
    };
  };

  getCoord = function(boardPoint, sett, x, y) {
    x -= boardPoint.x;
    y -= boardPoint.y;
    x /= sett.cellsize;
    y /= sett.cellsize;
    x = Math.floor(x);
    y = Math.floor(y);
    return {
      col: x,
      row: y
    };
  };

  getField = function(sett, player, _arg) {
    var col, row;
    col = _arg.col, row = _arg.row;
    if (col < 0 || row < 0 || col >= sett.cellsize || row >= sett.cellsize) {
      return false;
    } else {
      return sett[player].board[col][row];
    }
  };

  output = function(text) {
    var out;
    out = document.getElementById('output');
    if (out) {
      return out.value += text;
    }
  };

  handleRightClick = function(stage, sett, coord) {
    var field;
    if (sett.ingame) {
      if (sett.choosing) {
        field = getField(sett, 'player', coord);
        if (field) {
          if (field.chosen) {
            return;
            sett.shipsleft++;
          } else {
            sett.shipsleft--;
          }
          field.house.alpha = 1 - field.house.alpha;
          field.chosen = !field.chosen;
          showShipsLeft(stage, sett);
        }
        if (sett.shipsleft === 0) {
          sett.choosing = false;
          setStatusText(stage, sett, 'Attack the enemy!');
          initializeComputer(stage, sett);
          addTargetIcons(stage, sett, 'player');
          addTargetIcons(stage, sett, 'computer');
        }
      }
    }
    return stage.update();
  };

  forEachBoard = function(sett, player, fun) {
    var i, j, row, x, _i, _len, _ref, _results;
    _ref = sett[player].board;
    _results = [];
    for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
      row = _ref[i];
      _results.push((function() {
        var _j, _len1, _results1;
        _results1 = [];
        for (j = _j = 0, _len1 = row.length; _j < _len1; j = ++_j) {
          x = row[j];
          if (x) {
            _results1.push(fun(x, {
              col: i,
              row: j
            }));
          } else {
            _results1.push(void 0);
          }
        }
        return _results1;
      })());
    }
    return _results;
  };

  getOponent = function(player) {
    if (player === 'computer') {
      return 'player';
    } else {
      return 'computer';
    }
  };

  copyListOfCoord = function(list) {
    var x, _i, _len, _results;
    _results = [];
    for (_i = 0, _len = list.length; _i < _len; _i++) {
      x = list[_i];
      _results.push({
        row: x.row,
        col: x.col
      });
    }
    return _results;
  };

  initializeComputer = function(stage, sett, player) {
    var allcoord, col, field, oponent, p, row, _i, _len, _ref, _ref1;
    if (player == null) {
      player = 'computer';
    }
    oponent = getOponent(player);
    allcoord = getContinentCoordinates(sett[player].continent);
    shuffle(allcoord);
    sett[player].ships = allcoord.slice(0, +(sett.totalships - 1) + 1 || 9e9);
    _ref = sett[player].ships;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      p = _ref[_i];
      console.log(p.row, p.col);
      _ref1 = [p.row + 1, p.col + 1], row = _ref1[0], col = _ref1[1];
      output("[" + col + ", " + row + "]\n");
      field = getField(sett, player, p);
      field.chosen = true;
      field.house.alpha = 0.5;
    }
    sett[player].moves = getContinentCoordinates(sett[oponent].continent);
    return shuffle(sett[player].moves);
  };

  addTargetIcons = function(stage, sett, player) {
    var i, numbers, _i, _ref, _results;
    numbers = (function() {
      _results = [];
      for (var _i = 1, _ref = sett.totalships; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--){ _results.push(_i); }
      return _results;
    }).apply(this);
    shuffle(numbers);
    i = 0;
    return forEachBoard(sett, player, function(field, coord) {
      var icon, name, point;
      if (field.chosen) {
        point = getXY(sett, coord, sett[player].continent.boardPoint);
        name = 'target' + numbers[i] + '.png';
        i++;
        icon = createIcon(name, point, sett, 'img/');
        icon.alpha = 0;
        field['fire'] = icon;
        return stage.addChild(icon);
      }
    });
  };

  getContinentCoordinates = function(continent) {
    return copyListOfCoord(coordinates[continent.name]);
  };

  getAllCoordinates = function(upto) {
    var allcoord, x, y, _i, _j;
    allcoord = [];
    for (x = _i = 0; 0 <= upto ? _i <= upto : _i >= upto; x = 0 <= upto ? ++_i : --_i) {
      for (y = _j = 0; 0 <= upto ? _j <= upto : _j >= upto; y = 0 <= upto ? ++_j : --_j) {
        allcoord.push({
          row: x,
          col: y
        });
      }
    }
    return allcoord;
  };

  randomInt = function(from, to) {
    return Math.floor(Math.random() * to) + from;
  };

  shuffle = function(a) {
    var i, j, _i, _ref, _ref1;
    if (a.length < 2) {
      return;
    }
    for (i = _i = _ref = a.length - 1; _ref <= 1 ? _i <= 1 : _i >= 1; i = _ref <= 1 ? ++_i : --_i) {
      j = Math.floor(Math.random() * (i + 1));
      _ref1 = [a[j], a[i]], a[i] = _ref1[0], a[j] = _ref1[1];
    }
    return a;
  };

  handleLeftClick = function(stage, sett, coord) {
    var field;
    if (sett.ingame) {
      if (sett.choosing) {
        setStatusText(stage, sett, 'You cannot fire :-(. Place your ships first.');
      } else {
        field = getField(sett, 'computer', coord);
        if (field) {
          if (field.hit) {
            setStatusText(stage, sett, "You have already fired there! Don't waste your ammo!");
          } else {
            field.hit = true;
            if (field.chosen) {
              field.house.alpha = 0;
              field.fire.alpha = 1;
              sett['computer'].health--;
              if (sett['computer'].health) {
                setStatusText(stage, sett, 'Nice one!');
              } else {
                showWinner(stage, sett, 'player');
              }
            } else {
              field.fire.alpha = 1;
              setStatusText(stage, sett, 'You missed!');
            }
            if (sett.ingame) {
              playComputer(stage, sett);
            }
          }
        }
      }
    }
    return stage.update();
  };

  showWinner = function(stage, sett, player) {
    var numcomp, numplayer;
    setStatusText(stage, sett, sett.descriptions.winner[player]);
    sett.ingame = false;
    numcomp = showDeadPeople(sett, 'computer');
    numplayer = showDeadPeople(sett, 'player');
    doBoth(function(player) {
      sett[player].records = getCV(sett, player);
      return saveRecords(sett, player, sett[player].records);
    });
    animateToResults(sett);
    if (!sett.playerVsComputer) {
      console.log(sett.playerVsComputer);
      return setTimeout((function() {
        return animateToTop(stage, sett);
      }), 10000);
    }
  };

  countDeadChildren = function(sett, player) {
    var counter;
    counter = 0;
    forEachBoard(sett, player, function(field) {
      if (field.hit && !field.chosen) {
        return counter++;
      }
    });
    return counter;
  };

  saveRecords = function(sett, player, records) {
    var computerDeaths, html, id, playerDeaths, rec, title, _i, _len;
    playerDeaths = wordNumbers[countDeadChildren(sett, 'player')];
    computerDeaths = wordNumbers[countDeadChildren(sett, 'computer')];
    id = "#cv" + player;
    if (player === "computer") {
      title = "During the destruction of enemy targets you did not hit " + computerDeaths + " times! Those " + computerDeaths + " hits killed " + computerDeaths + " innocent children!";
    } else {
      title = "Those " + playerDeaths + " hits killed " + playerDeaths + " innocent children!";
    }
    html = "<b>" + title + "</b><ul>";
    for (_i = 0, _len = records.length; _i < _len; _i++) {
      rec = records[_i];
      html += "<li>" + rec + "</li>";
    }
    html += "</ul>";
    return $(id).html(html);
  };

  getCV = function(sett, player) {
    var states;
    states = {};
    forEachBoard(sett, player, function(field, coord) {
      var temp;
      if (field.hit && !field.chosen) {
        temp = field.states[0];
        if (states[temp]) {
          return states[temp]++;
        } else {
          return states[temp] = 1;
        }
      }
    });
    console.log(states);
    return getCVRecords(states);
  };

  getCVRecords = function(states) {
    var count, records, state, temp, x, _i, _len, _ref;
    records = [];
    for (state in states) {
      count = states[state];
      temp = cv[state];
      if (!temp) {
        temp = ["temporary child", "temporary child", "temporary child", "temporary child", "temporary child", "temporary child", "temporary child", "temporary child", "temporary child", "temporary child", "temporary child", "temporary child"];
      }
      shuffle(temp);
      _ref = temp.slice(0, +(count - 1) + 1 || 9e9);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        x = _ref[_i];
        records.push(x + ", " + state);
      }
    }
    return records;
  };

  animateToResults = function(sett) {
    return setTimeout((function() {
      if (!sett.playerVsComputer) {
        $("#backtotop").hide();
      }
      $("#cv").show();
      return $('html, body').animate({
        scrollTop: $("#cv").offset().top
      }, 4000);
    }), 2500);
  };

  animateToTop = function(stage, sett) {
    $('html, body').animate({
      scrollTop: $("#game").offset().top
    }, 2000);
    return setTimeout((function() {
      return vanishGame(stage, sett);
    }), 2250);
  };

  vanishGame = function(stage, sett) {
    var anim, child, container, count, _i, _len, _ref;
    container = [];
    _ref = stage.children;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      child = _ref[_i];
      if (child !== sett.backgroundLight) {
        container.push(child);
      }
    }
    stage.update();
    count = 20;
    return anim = setInterval((function() {
      var c, words, _j, _len1;
      for (_j = 0, _len1 = container.length; _j < _len1; _j++) {
        c = container[_j];
        c.alpha -= 0.05;
      }
      count--;
      stage.update();
      if (count === 0) {
        clearInterval(anim);
        words = showFinalWords(stage, sett);
        return alphaAnimate(stage, sett, words, 0.05, 0, 1, function() {
          var _k, _len2;
          for (_k = 0, _len2 = container.length; _k < _len2; _k++) {
            c = container[_k];
            stage.removeChild(c);
          }
          return setTimeout((function() {
            return alphaAnimate(stage, sett, words, -0.05, 1, 0, (function() {
              var pb;
              stage.removeChild(words);
              pb = createReverseProgressBar(stage);
              return showReverseProgress(stage, sett, pb);
            }));
          }), 5000);
        });
      }
    }), 50);
  };

  showFinalWords = function(stage, sett) {
    var text;
    text = new createjs.Text('This game was created on the occasion of the \nInternational Day of Innocent Children Victims of Aggression.\nThe day was recognized in 1983 by UN.', "25px Helvetica", "#4b4a4a");
    text.x = 500;
    text.y = 200;
    text.alpha = 0;
    text.textAlign = 'center';
    stage.addChild(text);
    stage.update();
    return text;
  };

  max = function(a, b) {
    if (a > b) {
      return a;
    } else {
      return b;
    }
  };

  showReverseProgress = function(stage, sett, progressBar) {
    var anim, count, rect;
    rect = new createjs.Shape();
    stage.addChild(rect);
    count = 60;
    return anim = setInterval((function() {
      rect.graphics.beginFill("white").drawRect(182, 302, 596, 26);
      rect.graphics.beginFill("black").drawRect(182, 302, max(10 * count - 4, 0), 26);
      stage.update();
      count--;
      if (count < 0) {
        stage.removeChild(rect);
        stage.removeChild(progressBar);
        clearInterval(anim);
        return vanishStage(stage, sett);
      }
    }), 100);
  };

  vanishStage = function(stage, sett) {
    var anim, count;
    count = 10;
    return anim = setInterval((function() {
      var c, _i, _len, _ref;
      if (count < 0) {
        clearInterval(anim);
        stage.update();
        $('#cv').hide();
        if (!sett.playerVsComputer) {
          return setTimeout((function() {
            playsCounter++;
            console.log(playsCounter);
            if (playsCounter === 50) {
              window.location.href = window.location.href;
            }
            stage = new createjs.Stage("game");
            return initializeGame(stage, false);
          }), 2000);
        }
      } else {
        count--;
        _ref = stage.children;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          c = _ref[_i];
          c.alpha -= 0.1;
        }
        return stage.update();
      }
    }), 50);
  };

  createReverseProgressBar = function(stage) {
    var rect;
    rect = new createjs.Shape();
    rect.graphics.beginFill("black").drawRect(180, 300, 600, 30);
    stage.addChild(rect);
    stage.update();
    return rect;
  };

  showDeadPeople = function(sett, player) {
    return forEachBoard(sett, player, function(field, coord) {
      if (field.hit && !field.chosen) {
        field.fire.alpha = 0;
        return field.cross.alpha = 1;
      }
    });
  };

  moveCoord = function(origin, shift) {
    return new createjs.Point(origin.x + shift.x, origin.y + shift.y);
  };

  playComputer = function(stage, sett, player) {
    if (player == null) {
      player = 'computer';
    }
    sett.ingame = false;
    placeField(stage, sett, sett[player].moves[sett[player].movesCount], player);
    return sett[player].movesCount++;
  };

  placeField = function(stage, sett, coord, player) {
    var animation, field, fun, oponent;
    if (player == null) {
      player = 'computer';
    }
    oponent = getOponent(player);
    field = getField(sett, oponent, coord);
    field.hit = true;
    if (field.chosen) {
      sett[oponent].health--;
    }
    animation = animateThinking(stage, sett);
    fun = function() {
      stopAnimateThinking(sett, animation);
      field.house.alpha = 0;
      field.fire.alpha = 1;
      if (sett[oponent].health === 0) {
        sett.ingame = false;
        showWinner(stage, sett, player);
      }
      if (field.chosen) {
        setStatusText(stage, sett, "Enemy destroyed one of your target!");
      }
      stage.update();
      if (sett.ingame && !sett.playerVsComputer) {
        return playComputer(stage, sett, oponent);
      }
    };
    return sett.animations.push(setTimeout(fun, randomInt(100, 200)));
  };

  animateThinking = function(stage, sett) {
    var animation, index, length;
    index = 0;
    length = sett.thinking.length;
    animation = setInterval((function() {
      var x, _i, _len, _ref;
      _ref = sett.thinking;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        x = _ref[_i];
        x.alpha = 0;
      }
      sett.thinking[index].alpha = 1;
      stage.update();
      return index = (index + 1) % length;
    }), 100);
    sett.animations.push(animation);
    return animation;
  };

  stopAnimateThinking = function(sett, animation) {
    var x, _i, _len, _ref;
    _ref = sett.thinking;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      x = _ref[_i];
      x.alpha = 0;
    }
    clearInterval(animation);
    return sett.ingame = true;
  };

  createStatusBar = function(stage, sett) {
    var text;
    text = new createjs.Text('Vitejte ve hre', "20px Helvetica", "#4b4a4a");
    text.x = 600;
    text.y = 600;
    stage.addChild(text);
    return sett.statusbar = text;
  };

  setStatusText = function(stage, sett, text) {
    sett.statusbar.text = text;
    return stage.update();
  };

  showShipsLeft = function(stage, sett) {
    return setStatusText(stage, sett, sett.descriptions.putships + sett.shipsleft);
  };

  window.onload = function() {
    return startNewGame();
  };

}).call(this);
