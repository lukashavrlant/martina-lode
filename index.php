<html>
<head>
    <meta charset="utf-8">
    <title>Bitter sugar game | Martina Pavelková</title>

    <style type="text/css">
    html, body {
        border: 0;
        padding: 0;
        background-color: #fafafa;
        font-family: "helvetica";
        color: rgb(75, 74, 74);
    }
    #game {
        /*border: 1px solid black;*/
    }

    .computer {
        color: red;
    }

    #content {
        width: 960px;
        margin: 0 auto;
    }

    .player {
        color: blue;
    }

    #cv {
        display: none;
        margin-top: 30px;
        width: 960px;
        margin-left: auto;
        margin-right: auto;
    }

    #cvcomputer {
        margin-left: 25px;
        width: 420px;
        float: left;
    }

    #cvplayer {
        margin-right: 25px;
        width: 420px;
        float: right;
    }

    #statusbar {
        width: 580px;
        text-align: center;
        font-weight: bold;
    }

    #bean {
        position: absolute;
        top: 0;
        left: -200px;
    }
    </style>
    <script type="text/javascript">
var playsCounter = 0;
var playerVsComputer = <?php
if (isset($_GET['auto'])) {
    echo 'false;';
} else {
    echo 'true;';
}
?>
    </script>
    <script src="extlib/easeljs-0.6.1.min.js"></script>
    <script src="extlib/preloadjs-0.3.1.min.js"></script>
    <script src="extlib/coordinates.js"></script>
    <script src="extlib/jquery-1.10.1.min.js"></script>
    <script src="extlib/cv.js"></script>
    <script src="lib/game.js"></script>
</head>
<body id='body'>
    <div id='page'>
        <div id='header'>

        </div>
<br><br>
        <div id='content'>
            <canvas width="960" height="720" id='game'>
                <img src="img/loader.gif">
            </canvas>
        </div>

        <!-- <div id='statusbar'></div> -->
        <!-- <textarea id='output' rows=5></textarea> -->
        <!-- <textarea id='output2'></textarea> -->

<!-- <input type="button" id="button" value="scroll"> -->
        <div id='cv'>

<div id="cvcomputer">
<b>During the destruction of enemy targets you did not hit seven times! Those seven hits killed seven innocent children!</b>
<ul>
    <li>Bohumír Bunža kněz 17. října 1950 zemřel na následky mučení při vysleších <li>
Bohumil Burian kněz, farář ve Velkém Meziříčí 29. dubna 1958 zemřel po výslechu církevním tajemníkem <li>
Bohuslav Burian kněz 29. dubna 1959 zemřel ve vězení v důsledku mučení a neposkytnutí lékařské péče <li>
Michal Buzalka biskup trnavský 7. prosince 1961 zemřel v internaci Přemysl Coufal tajný kněz 23/24. dubna 1981 zavražděn nebo dohnán k sebevraždě <li>
Josef Čihák kněz 18. února 1960 zemřel ve vězeňské nemocnici Václav Drbola kněz 3. srpna 1951 justiční vražda
</ul>
</div>

<div id="cvplayer">
<b>During the defence your targets allowed you die ten innocent children!</b>
<ul>
<li>Andrej Gerát kněz 4. července 1954 zemřel ve vězeňském oddělení fakultní nemocnice v Brně <li>
Josef Faustín Hartl milosrdný bratr leden 1952 zemřel ve vězeňské nemocnici <li>
Cypriána Františka Hlavínová představená Chudých školských sester 23. listopadu 1950 zemřela ve vězeňské nemocnici bl. <li>
Vasiľ Hopko řeckokatolický biskup, pomocný biskup prešovský 23. července 1976 zemřel na následky předchozího věznění <li>
Josef Chadraba kněz 16. dubna 1956 zemřel ve vězení <li>
Štefan Ivančo řeckokatolický kněz 1951 zemřel v TNP <li>
Josef Jakubec kněz 27. června 1955 zemřel ve vězení <li>
Bohuslav Stanislav Jarolímek kněz, premonstrát, opat strahovského kláštera 31. ledna 1951 zemřel ve vězeňské nemocnici <li>
Adolf Kajpr kněz, jezuita 17. září 1959 zemřel ve vězení v důsledku odepření lékařské péče <li>
Josef Kašík kněz, salesián 12. prosince 1957 zemřel na následky zranění ze stavby Křímovské přehrady
</ul>
</div>

<div style="clear:both">&nbsp;</div>

<div style="text-align: center;">
    <input type="button" value="Continue..." id="backtotop">
</div>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

<script type="text/javascript">
$("#button").click(function() {
     $('html, body').animate({
         scrollTop: $("#cv").offset().top
     }, 2000);
 });
    </script>
        </div>
    </div>
</body>
</html>